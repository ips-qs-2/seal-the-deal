/**
 * @module util
 * Module containing multiple utility functions
 *
 * @package root
 *
 * @returns following methods:
 * @public exposeRequest
 *
 * @author Seal the Deal
 * @version 1.0.1
 * @date 2020/11/27
 * @lastrevision
 *
 */
'use strict'

// Modules

// variables for calculations
const QUANTIDADE_MINIMA_CASAS = 1
const direcoes = ['N', 'S', 'E', 'O', 'NE', 'SE', 'NO', 'SO']
const LIMITE_SUPERIOR = 0
const LIMITE_INFERIOR = 10
const LIMITE_ESQUERDO = 0
const LIMITE_DIREITO = 16
const MAX_Y_COORDINATE = 11
const MAX_X_COORDINATE = 10

/**
 *
 * @param {object} pos1
 * @param {object} pos2
 * @param {integer} speed
 */
export const isInRange = (pos1, pos2, speed) => {
  const xPosDifference = Math.abs(
    parseFloat(pos1.xPos) - (parseFloat(pos2.xPos) + 0.5)
  )
  const yPosDifference = Math.abs(
    parseFloat(pos1.yPos) - (parseFloat(pos2.yPos) + 0.5)
  )
  return (
    xPosDifference >= speed + 1.5 || yPosDifference >= speed + 1.5
  )
}

/**
 * Returns JSON parsed string or null
 * @param {string} message
 */
export const getJson = str => {
  let message
  try {
    message = JSON.parse(str)
  } catch (e) {
    message = { badFormat: str }
  }
  return message
}

/**
 * Array extension with a Synchronous Foreach
 * (Construtor parasita)
 * @public
 * @param {array} arrayToExtend : Array to extend. Null for an empty array
 */
export const ArrayEx = function(arrayToExtend = null) {
  // Criar o Array
  const values = !arrayToExtend ? [] : [...arrayToExtend]

  /**
   * Parasite function
   * @param {function} fnCallback
   */
  values.syncForEach = function(fnCallback) {
    const iterator = i => {
      if (values.length > i) {
        fnCallback(values[i], i, values)
        iterator(i + 1)
      }
    }
    iterator(0)
  }
  // retorna o Array
  return values
}

/**
 * @public
 * Converts position string to position object
 * Example
 * @param {string} positionString
 * @returns {object} : { xPos: "integer", yPos: "integer" }
 */
export const getPosition = positionString => {
  // Regex pattern to match x value. Example: X6Y5 matches 6
  const xPosPattern = /(?!X)\d+(?=Y\d+)/i
  // Regex pattern to match y value. Example: X6Y5 matches 5
  const yPosPattern = new RegExp(/\d+$/i)

  const matchX = xPosPattern.exec(positionString)
  const matchY = yPosPattern.exec(positionString)

  return matchX && matchY
    ? { xPos: matchX[0], yPos: matchY[0] }
    : null
}

/**
 * Gerar um numero aleatorio
 * @param {number} valorMaximo
 */
function generate(valorMaximo) {
  return Math.round(Math.random() * valorMaximo)
}

/**
 * Gerar uma posição nãoa limitada
 */
export const generateUnrestricted = () => {
  let xPos = generate(MAX_X_COORDINATE)
  let yPos = generate(MAX_Y_COORDINATE)

  return { xPos, yPos }
}

/**
 * Geraruma posição limitada por outra posição
 * @param {object} pontos
 */
export const generateRestrict = function generateRestrict(pontos) {
  let ponto = generateUnrestricted()

  for (let p of pontos) {
    let ehIgual = ponto.xPos === p.xPos && ponto.yPos === p.yPos

    if (ehIgual) {
      return generateRestrict(pontos)
    }
  }

  return ponto
}

/**
 * Calcula a distância entre duas posições
 * @param {object} a
 * @param {object} b
 */
export const calculateDistance = (a, b) => {
  if (a == null || b == null) {
    return 99999999
  }
  a.xPos = parseInt(a.xPos)
  a.yPos = parseInt(a.yPos)
  b.xPos = parseInt(b.xPos)
  b.yPos = parseInt(b.yPos)
  let catetoX = Math.abs(a.xPos - b.xPos)
  let catetoY = Math.abs(a.yPos - b.yPos)
  let soma = catetoX * catetoX + catetoY * catetoY
  let hipotenusa = Math.sqrt(soma)
  return Math.floor(hipotenusa)
}

/**
 * Indica a direção de um ponto para o outro
 * @param {object} origin
 * @param {object} destiny
 */
export const indicateDirection = (origin, destiny) => {
  if (origin.xPos < destiny.xPos) {
    if (origin.yPos > destiny.yPos) {
      return 'NE'
    }

    if (origin.yPos === destiny.yPos) {
      return 'E'
    }

    if (origin.yPos < destiny.yPos) {
      return 'SE'
    }
  }

  if (origin.xPos === destiny.xPos) {
    if (origin.yPos > destiny.yPos) {
      return 'N'
    }

    if (origin.yPos < destiny.yPos) {
      return 'S'
    }
  }

  if (origin.xPos > destiny.xPos) {
    if (origin.yPos > destiny.yPos) {
      return 'NO'
    }

    if (origin.yPos === destiny.yPos) {
      return 'O'
    }

    if (origin.yPos < destiny.yPos) {
      return 'SO'
    }
  }

  return null
}

/**
 * Obter o raio
 * @param {number} radius
 * @param {number} distance
 */
export const getDistanceRadius = (radius, distance) => {
  if (distance < radius) {
    return distance
  } else {
    return radius
  }
}

/**
 * Mover um ponto
 * @param {object} ponto
 * @param {number} casas
 * @param {string} direcao
 */
export const move = (ponto, casas, direcao) => {
  if (
    casas >= QUANTIDADE_MINIMA_CASAS &&
    direcoes.includes(direcao)
  ) {
    for (let i = 0; i < direcao.length; i++) {
      let letra = direcao.charAt(i)

      if (letra === 'N') {
        moveParaCima(ponto, casas)
      }

      if (letra === 'S') {
        moveParaBaixo(ponto, casas)
      }

      if (letra === 'E') {
        moveParaDireita(ponto, casas)
      }

      if (letra === 'O') {
        moveParaEsquerda(ponto, casas)
      }
    }
  }
}

/**
 * Move para cima
 * @param {object} ponto
 * @param {object} casas
 */
function moveParaCima(ponto, casas) {
  if (ponto.yPos > LIMITE_SUPERIOR) {
    let resultado = ponto.yPos - casas

    ponto.yPos =
      resultado < LIMITE_SUPERIOR ? LIMITE_SUPERIOR : resultado
  }
}

/**
 * Move para baixo
 * @param {object} ponto
 * @param {object} casas
 */
function moveParaBaixo(ponto, casas) {
  if (ponto.yPos < LIMITE_INFERIOR) {
    let resultado = ponto.yPos + casas

    ponto.yPos =
      resultado > LIMITE_INFERIOR ? LIMITE_INFERIOR : resultado
  }
}

/**
 * Move para a direita
 * @param {object} ponto
 * @param {object} casas
 */
function moveParaDireita(ponto, casas) {
  if (ponto.xPos < LIMITE_DIREITO) {
    let resultado = ponto.xPos + casas

    ponto.xPos =
      resultado > LIMITE_DIREITO ? LIMITE_DIREITO : resultado
  }
}

/**
 * Move para a esquerda
 * @param {object} ponto
 * @param {object} casas
 */
function moveParaEsquerda(ponto, casas) {
  if (ponto.xPos > LIMITE_ESQUERDO) {
    let resultado = ponto.xPos - casas

    ponto.xPos =
      resultado < LIMITE_ESQUERDO ? LIMITE_ESQUERDO : resultado
  }
}

/**
 * Abreviar uma string
 * @param {string} str
 * @param {number} max
 * @param {string} suffix
 */
export const abbreviate = (str, max, suffix) => {
  if (
    (str = str
      .replace(/^\s+|\s+$/g, '')
      .replace(/[\r\n]*\s*[\r\n]+/g, ' ')
      .replace(/[ \t]+/g, ' ')).length <= max
  ) {
    return str
  }
  let abbr = ''
  str = str.split(' ')
  suffix = typeof suffix !== 'undefined' ? suffix : '...'
  max = max - suffix.length

  for (let len = str.length, i = 0; i < len; i++) {
    if ((abbr + str[i]).length < max) {
      abbr += str[i] + ' '
    } else break
  }
  return abbr.replace(/[ ]$/g, '') + suffix
}
