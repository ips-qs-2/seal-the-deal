/**
 * @module Player
 * @description
 *      Responsible for managing the state of the player in the game
 *
 *
 * @package src.vuex
 * @public
 *
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */
import Vue from 'vue'
import Vuex from 'vuex'
import * as settings from '../../settings.json'

Vue.use(Vuex)

/**
 *  Store is the main constant for state management in the application
 */
const store = new Vuex.Store({
  state: {
    sessionId: null,
    connection: null,
    style: 4,
    language: 'pt',
    result: null,
    player: {
      username: null,
      birthday: '',
      email: '',
      avatar: '',
      credits: 0,
      points: 0
    },
    game: {
      inQueue: false,
      isOn: false,
      me: {
        speed: null,
        turnToPlay: false,
        xPos: null,
        yPos: null,
        lifetime: null,
        mode: null,
        lifes: null,
        // num powerups
        powerUps: {
          speed: null,
          radar: null,
          scent: null,
          seal: null
        },
        powerUpsToUse: [], // func, func, func
        powerUpsRollback: [] // func, func, func
      },
      opponent: {
        xPos: null,
        yPos: null,
        lifetime: null,
        mode: null,
        lifes: null,
        visible: false,
        avatar: ''
      },
      // sharks: [ { xPos: integer, yPos: integer } ]
      sharks: [
        {
          xPos: 3,
          yPos: 3
        },
        {
          xPos: 3,
          yPos: 14
        },
        {
          xPos: 6,
          yPos: 8
        }
      ]
    },
    store: {
      seal: null,
      speed: null,
      scent: null,
      radar: null
    },
    classify: null,
    rank: null
  },
  mutations: {
    // functions to commit and make state actions
    setSessionId(state, sessionId) {
      state.sessionId = sessionId
    },
    setMode(state, mode) {
      state.game.me.mode = mode
    },
    setStyle(state, style) {
      state.style = style
      const innerBoxes = document.getElementsByClassName(
        'innerSquare'
      )
      const outerBoxes = document.getElementsByClassName(
        'outerSquare'
      )
      if (innerBoxes.length > 0) {
        Array.from(innerBoxes).forEach(element => {
          element.style.backgroundColor =
            settings.styles[this.state.style].seacolor
        })
      }
      if (outerBoxes.length > 0) {
        Array.from(outerBoxes).forEach(element => {
          element.style.backgroundColor =
            settings.styles[this.state.style].seacolor
        })
      }
    },
    setPlayer(state, player) {
      state.player = player
    },
    setMe(state, me) {
      state.game.me = me
    },
    setOpponent(state, opponent) {
      state.game.opponent = opponent
    },
    setConnection(state, connection) {
      state.connection = connection
    },
    setSharks(state, sharks) {
      state.game.sharks = sharks
    },
    setStore(state, store) {
      state.store = store
    },
    setClassify(state, classify) {
      state.classify = classify
    },
    setRankikg(state, rank) {
      state.rank = rank
    }
  },
  getters: {
    // state values changes
    getState: state => state,
    getSessionId: state => state.sessionId,
    getPlayer: state => state.player,
    getMe: state => state.game.me,
    getOpponent: state => state.game.opponent,
    getConnection: state => state.connection,
    getSharks: state => state.game.sharks,
    getStore: state => state.store
  },
  actions: {
    setSessionId(state, sessionId) {
      state.commit('setSessionId', sessionId)
    },
    setMode(state, mode) {
      state.commit('setMode', mode)
    },
    setStyle(state, style) {
      state.commit('setStyle', style)
    },
    setPlayer(state, player) {
      state.commit('setPlayer', player)
    },
    setMe(state, me) {
      state.commit('setMe', me)
    },
    setOpponent(state, opponent) {
      state.commit('setOpponent', opponent)
    },
    setConnection(state, connection) {
      state.commit('setConnection', connection)
    },
    setSharks(state, sharks) {
      state.commit('setSharks', sharks)
    },
    setStore(state, store) {
      state.commit('setStore', store)
    },
    setClassify(state, classify) {
      state.commit('setStore', classify)
    },
    setRanking(state, rank) {
      state.commit('setStore', rank)
    }
  }
})
export { store }
