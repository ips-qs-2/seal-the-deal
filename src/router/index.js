import Vue from 'vue'
import Router from 'vue-router'
import Board from '../components/Board'
import StartGame from '../components/StartGame'
import Home from '../components/Home'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/Board',
      name: 'Board',
      component: Board
    },
    {
      path: '/player/',
      name: 'StartGame',
      component: StartGame
    }
  ]
})
