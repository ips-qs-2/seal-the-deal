/**
 * @module backend-ws
 *      Module responsible for stablish conversation
 *      with the backend through WebSockets
 *
 *
 * @package src
 *
 * @returns following methods:
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */

// Import modules and constants
import { getJson } from '../util'
import GameBusiness from '../GameBusiness'
import * as settings from '../../settings.json'
const Game = new GameBusiness()

export const WebSocketServer = function(sessionId, store, router) {
  console.log('Starting Connection to WebSocket Server')

  // Creates object to export
  const obj = {}

  // Access to app state object
  const state = store.getters.getState

  // Initialize connection to backend websocket server
  const wws = new WebSocket(
    `${settings.websockets.server}/${sessionId}/`
    // `${'ws://localhost:5000/player'}/${sessionId}/`
  )

  /**
   * Interface for sending commands to the backend
   * @public
   * @param {string} command
   * @param {object} data
   */
  obj.send = (command, data) => {
    wws.send(JSON.stringify({ command: command, data: data }))
  }

  /**
   * @public
   * Send message to get user
   */
  obj.getPlayer = () => {
    wws.send(JSON.stringify({ command: 'getPlayer' }))
  }

  /**
   * @public
   * Send message to get store
   */
  obj.getStore = () => {
    wws.send(JSON.stringify({ command: 'getStore' }))
  }
  /**
   * @public
   * Send message to get ranking
   */
  obj.getRanking = () => {
    wws.send(JSON.stringify({ command: 'getRanking' }))
  }
  /**
   * @public
   * Send message to get ranking
   */
  obj.getClassify = () => {
    wws.send(JSON.stringify({ command: 'getClassify' }))
  }

  /**
   * @public
   * Send message to request a game start and4
   */
  obj.startMatch = () => {
    wws.send(
      JSON.stringify({
        command: 'startMatch',
        data: { mode: state.game.me.mode }
      })
    )
  }

  /**
   * Make a powerup transaction request to backend
   * @param {object} powerups : [{id: 1, amount: 3}, {id: 3, amount: 2}, ...]
   */
  obj.buyPowerup = powerups => {
    wws.send(
      JSON.stringify({
        command: 'buyPowerup',
        data: {
          powerups: powerups
        }
      })
    )
  }

  /**
   * Consumes a powerup
   * @param {string} powerup
   */
  obj.usePowerup = powerup => {
    wws.send(
      JSON.stringify({
        command: 'usedPowerup',
        powerup: powerup
      })
    )
  }

  /**
   * @private
   * Sets classification state
   * @param {object} event
   */
  obj.setClassify = event => {
    state.classify = event.data
  }

  /**
   * @private
   * Sets ranking state
   * @param {object} event
   */
  obj.setRanking = event => {
    state.rank = event.data
  }

  /**
   * Deal with the timeout event, passes turn
   * @param {object} event
   */
  const turnTimeout = event => {
    Game.setMyMove()
    wws.send(JSON.stringify({ command: 'ok', echo: event }))
  }

  /**
   * @private
   * Sets user from the event data
   * @param {object} event
   */
  const setPlayer = event => {
    state.player = {
      username: event.data.name,
      birthday: event.data.birthday,
      email: event.data.email,
      avatar: event.data.avatar,
      credits: event.data.credits,
      points: event.data.points
    }
    state.game.me.powerUps.seal = event.data.powerups.seal
    state.game.me.powerUps.scent = event.data.powerups.scent
    state.game.me.powerUps.speed = event.data.powerups.speed
    state.game.me.powerUps.radar = event.data.powerups.radar
    wws.send(JSON.stringify({ command: 'ok', echo: event }))
  }

  /**
   * @private
   * Sets store prices from the event data
   * @param {object} event
   */
  const setStore = event => {
    event.data.forEach(powerup => {
      state.store[powerup.name] = powerup
    })
    wws.send(JSON.stringify({ command: 'ok', echo: event }))
  }

  /**
   *
   * @param {*} echo
   */
  const onOkEcho = function(echo) {
    switch (echo) {
      case 'startMatch':
        Game.setInQueue()
        break
      case 'buyPowerup':
        //
        break
    }
  }

  /**
   * Closes player session
   * @param {object} event
   */
  const connectionClosed = function(event) {
    state.player.username = null
    router.push('/')
  }

  /**
   * Dynamic listener
   * @param {object} event
   */
  obj.Getgameover = event => {}

  /**
   * @private
   * Actions to run on websocket connection success
   * Request objects to set state
   * @param {object} event
   */
  wws.onopen = function(event) {
    console.log('Successfully connected to WebSocket Server')
    obj.getPlayer()
    obj.getStore()
    obj.getClassify()
    obj.getRanking()
  }

  /**
   * On message event handler
   * @param {object} event
   */
  wws.onmessage = function(event) {
    event = getJson(event.data)
    switch (event.command) {
      case 'ok':
        state.result = event.command
        onOkEcho(event.echo)
        break
      case 'connectionClosed':
        console.log(event.message)
        connectionClosed(event)
        break
      case 'setConnectionTimeout':
        console.log(event)
        break
      case 'setPlayer':
        setPlayer(event)
        break
      case 'setStore':
        setStore(event)
        break
      case 'setClassify':
        obj.setClassify(event)
        wws.send(JSON.stringify({ command: 'ok', echo: event }))
        break
      case 'setRanking':
        obj.setRanking(event)
        wws.send(JSON.stringify({ command: 'ok', echo: event }))
        break
      case 'opponentPlay':
        Game.setOpponentMove(event.data)
        wws.send(JSON.stringify({ command: 'ok', echo: event }))
        break
      case 'setGameState':
        Game.setGameState(event.data)
        wws.send(JSON.stringify({ command: 'ok', echo: event }))
        break
      case 'startedMatch':
        Game.startedMatch(event.data)
        wws.send(JSON.stringify({ command: 'ok', echo: event }))
        break
      case 'turnTimeout':
        turnTimeout(event)
        break
      case 'sharksBytes':
        Game.processSharkBytes(event.data)
        wws.send(JSON.stringify({ command: 'ok', echo: event }))
        break
      case 'gameover':
        Game.over(event.data)
        obj.Getgameover(event)
        break
      case 'rejected':
        state.result = event.command
        break
    }
  }
  return obj
}
