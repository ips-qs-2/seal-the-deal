/**
 * @module GameBusiness
 * @description
 *      Responsible for exporting methods to deal with the game flow
 *      and needed calculations
 *
 * @package src
 * @public
 *
 *
 * @author Seal the Deal - Team
 * @version 1.0.1
 * @date 2020/11/28
 * @lastrevision
 *
 */
import {
  getPosition,
  calculateDistance,
  indicateDirection,
  getDistanceRadius,
  move,
  ArrayEx
} from './util'
import { store } from './vuex/Player'
import Vue from 'vue'
import Shark from './components/Shark'
import Blood from './components/Blood'
import * as settings from '../settings.json'

export default function() {
  const STATE = store.getters.getState
  const SHARK_QUANTITY = settings.sharks.spawn.quantity
  const SHARK_SPEED = settings.sharks.spawn.speed

  /**
   * Set the starting of match
   * @public
   * @param {object} data
   */
  this.startedMatch = data => {
    STATE.game.opponent = data.opponent
    STATE.game.isQueue = false
    STATE.game.isOn = true
    STATE.game.me.lifes = data.lifes || null
    STATE.game.me.turnToPlay = data.turnToPlay || false
    STATE.game.me.lifetime = data.lifetime || null
    STATE.game.me.speed =
      settings.player.mode[STATE.game.me.mode].speed
    STATE.game.sharks = data.sharks
  }

  /**
   * Set game state
   * @private
   * @param {@public} data
   */
  this.setGameState = data => {
    STATE.game.opponent.xPos = data.opponent.xPos
    STATE.game.opponent.yPos = data.opponent.yPos
    STATE.game.opponent.lifes = data.opponent.lifes
    STATE.game.me.xPos = data.me.xPos
    STATE.game.me.yPos = data.me.yPos
    STATE.game.me.lifes = data.me.lifes
    STATE.game.sharks = data.sharks
  }

  /**
   * @public
   * Set player move
   */
  this.setMyMove = () => {
    STATE.game.me.turnToPlay = false
    STATE.connection.send('play', {
      me: {
        xPos: STATE.game.me.xPos,
        yPos: STATE.game.me.yPos,
        lifetime: STATE.game.me.lifetime,
        lifes: STATE.game.me.lifes,
        powerUps: STATE.game.me.powerUps
      },
      sharks: STATE.game.sharks
    })
  }

  /**
   * Make sharks appear on the board
   * @public
   */
  this.spawnSharks = () => {
    let sharkPositions = STATE.game.sharks
    for (let i = 0; i < SHARK_QUANTITY; i++) {
      let squareSpawn = document.getElementById(
        `X${sharkPositions[i].xPos}Y${sharkPositions[i].yPos}`
      )
      const SharkConstructor = Vue.extend(Shark)
      const shark = new SharkConstructor().$mount()
      shark.$el.id = `shark${i}`
      squareSpawn.appendChild(shark.$el)
    }
  }

  /**
   * Move player
   * @param {DomElement} playerElement : player avatar
   * @param {DomElement} target : target div
   */
  this.movePlayer = (playerElement, target) => {
    const destination = getPosition(target.parentNode.id)
    target.appendChild(playerElement)
    STATE.game.me.xPos = destination.xPos
    STATE.game.me.yPos = destination.yPos
    // whenever the player moves, the sharks move also
    moveShark()
    // send move to backend
    this.setMyMove()
  }

  /**
   * Put seal on the board
   * @param {DomElement} seal
   * @param {DomElement} target
   */
  this.dropSeal = (seal, target) => {
    if (STATE.game.me.turnToPlay) {
      target.appendChild(seal)
      moveShark()
      STATE.connection.usePowerup('seal')
      this.setMyMove()
    }
  }

  /**
   * Saves opponent move to state
   * @public
   */
  this.setOpponentMove = () => {
    moveOpponent()
    STATE.game.me.turnToPlay = true
  }

  /**
   * Player is set to waiting for oppoenent to play
   * @public
   */
  this.setInQueue = () => {
    STATE.game.inQueue = true
  }

  /**
   * Render shark bytes blood
   * @public
   * @param {object} data
   */
  this.processSharkBytes = data => {
    if (data.me.length > 0) resetPlayerPosition()
    if (data.opponent.length > 0) resetOpponentPosition()

    const allBytes = [...data.me, ...data.opponent]
    allBytes.forEach(byteShark => {
      const sharks = document.getElementsByClassName('shark')
      // Add blood to any
      Array.from(sharks).forEach(shark => {
        const sharkBox = shark.parentNode
        if (
          sharkBox.id[1] === `${byteShark.xPos}` &&
          sharkBox.id[3] === `${byteShark.yPos}`
        ) {
          displayBlood(sharkBox)
        }
      })
    })
  }

  /**
   * Deal with game over, reset game state
   * @param {object} data
   */
  this.over = data => {
    STATE.game.me.turnToPlay = false
    if (!STATE.game.isOn) return
    STATE.result = data.result
    STATE.game.isOn = false
    resetOpponentPosition()
    resetPlayerPosition()
  }

  /**
   * Function to handle opponent movement
   * @private
   */
  const moveOpponent = () => {
    const opponent = document.getElementById('opponent')
    const opponentPosition = `X${STATE.game.opponent.xPos}Y${STATE.game.opponent.yPos}`
    const target = document.getElementById(opponentPosition)
    if (target) {
      target.appendChild(opponent)
    }
    for (let i = 0; i < SHARK_QUANTITY; i++) {
      const shark = document.getElementById(`shark${i}`)
      const sharkPosition = `X${STATE.game.sharks[i].xPos}Y${STATE.game.sharks[i].yPos}`
      const sharkTarget = document.getElementById(sharkPosition)
      if (sharkTarget) {
        sharkTarget.appendChild(shark)
      }
    }
  }

  /**
   * Move sharks to their closest targets
   * @public
   */
  const moveShark = () => {
    // Get all objects on the board
    const targets = new ArrayEx(getAllPlayersAndSealsPositions())
    const sharks = STATE.game.sharks

    // Calculate target for each shark
    for (let i = 0; i < SHARK_QUANTITY; i++) {
      const sharkPos = getSharkPositionById(`shark${i}`)
      const target = getClosestTargetOfPosition(sharkPos, targets)
      const distance = convertToSharkDistanceFrom(target, i)
      const direction = indicateDirection(sharkPos, target.position)
      const spaces = getDistanceRadius(SHARK_SPEED, distance)

      move(sharks[i], spaces, direction)
      if (sharks[i].xPos === 10) sharks[i].xPos = 9
      const newSharkSquare = document.getElementById(
        `X${sharks[i].xPos}Y${sharks[i].yPos}`
      )
      bleedIfContactWithObjects(targets, newSharkSquare, {
        xPos: sharks[i].xPos,
        yPos: sharks[i].yPos
      })
      if (newSharkSquare) {
        newSharkSquare.appendChild(
          document.getElementById(`shark${i}`)
        )
      }
    }
  }

  /**
   * Reset both players positions to original
   * @private
   */
  const resetOpponentPosition = () => {
    STATE.game.opponent.xPos = 11
    STATE.game.opponent.xPos = 8
    const opponent = document.getElementById('opponent')
    document.getElementById('X11Y8').appendChild(opponent)
  }

  /**
   * Reset both players positions to original
   * @private
   */
  const resetPlayerPosition = () => {
    STATE.game.me.xPos = 11
    STATE.game.me.xPos = 8
    const character = document.getElementById('character')
    document.getElementById('X11Y8').appendChild(character)
  }

  /**
   * Get closeste target of given position
   * @private
   * @param {object} position : position from which should find distance
   * @param {array} candidates : objects candidates as target
   */
  const getClosestTargetOfPosition = (position, candidates) => {
    let closestTarget = {
      id: '',
      position: { xPos: position.xPos, yPos: position.yPos },
      distance: 1000
    }
    candidates.syncForEach(target => {
      let dist = calculateDistance(position, target.position)
      if (dist < closestTarget.distance) {
        closestTarget.id = target.id
        closestTarget.position = target.position
        closestTarget.distance = dist
      }
    })

    return closestTarget
  }

  /**
   * Get all Players and Seals Positions
   * @private
   */
  const getAllPlayersAndSealsPositions = () => {
    const possibleTargets = new ArrayEx(getSealsPostions())
    possibleTargets.push({
      id: 'character',
      position: getMyPosition()
    })
    possibleTargets.push({
      id: 'opponent',
      position: getOpponentPosition()
    })
    return possibleTargets
  }

  /**
   * Get all seal positions
   * @private
   */
  const getSealsPostions = () => {
    // Build an extended array from all elements with class powerUp seal
    const seals = new ArrayEx(
      document.getElementsByClassName('powerUp seal')
    )
    const sealsPositions = []
    seals.syncForEach((seal, i) => {
      const parent = seal.parentNode
      let position = getPosition(parent.id)
      if (position !== null) {
        sealsPositions.push({ id: seal.id, position: position })
      }
    })
    return sealsPositions
  }

  /**
   * Get all shark by id number
   * @private
   */
  const getSharkPositionById = id =>
    getPosition(document.getElementById(id).parentNode.id)

  /**
   * Display blood on contact with humans or seals
   * @private
   * @param {array} targetsOnBoard : Targets on which it choose display blood
   * @param {DomElement} sharkParent : Parent node of the shark id the box (div) where he is at
   * @param {object} sharkPosition : Position on the shark
   */
  const bleedIfContactWithObjects = (
    targetsOnBoard,
    sharkParent,
    sharkPosition
  ) => {
    targetsOnBoard = new ArrayEx(targetsOnBoard)
    targetsOnBoard.syncForEach(t1 => {
      if (t1.position !== null) {
        if (calculateDistance(sharkPosition, t1.position) < 1) {
          displayBlood(sharkParent)
          targetsOnBoard.syncForEach(t2 => {
            if (t2.id.match(/^(seal)+\d+$/i)) {
              let objDomElement = document.getElementById(t2.id)
              objDomElement.parentNode.removeChild(objDomElement)
            }
          })
        }
      }
    })
  }

  /**
   * Remove blood instance
   * @param {DomElement[]} param0 : [ParentNode, BloodNode]
   */
  const removeBlood = ([parentNode, bloodNode]) => {
    parentNode.removeChild(bloodNode)
  }

  /**
   * Display blood on dom element target position
   * @private
   * @param {DomNode} target
   */
  const displayBlood = target => {
    const BloodConstructor = Vue.extend(Blood)
    const blood = new BloodConstructor().$mount()
    const bloodNode = blood.$el
    target.appendChild(bloodNode)
    setTimeout(removeBlood, 4000, [target, bloodNode])
  }

  /**
   * Convert distance to shark perpective
   * @private
   * @param {object} target
   */
  const convertToSharkDistanceFrom = (target, index) => {
    return target.distance + (index === 1 ? -2 : index === 2 ? -1 : 0)
  }

  /**
   * Retrieves current player position
   * @private
   */
  const getMyPosition = () =>
    getPosition(`X${STATE.game.me.xPos}Y${STATE.game.me.yPos}`)

  /**
   * Retrives opponent position
   * @private
   */
  const getOpponentPosition = () =>
    getPosition(
      `X${STATE.game.opponent.xPos}Y${STATE.game.opponent.yPos}`
    )
}
