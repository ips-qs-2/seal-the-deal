// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueWorker from 'vue-worker'
import { store } from './vuex/Player'
// Import Bootstrap an BootstrapVue CSS files
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// Make BootstrapVue available throughout your project

import {
  BootstrapVue,
  BootstrapVueIcons,
  FormSpinbuttonPlugin
} from 'bootstrap-vue'
Vue.use(FormSpinbuttonPlugin)
Vue.use(BootstrapVue)
Vue.use(VueWorker)
Vue.use(BootstrapVueIcons)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
