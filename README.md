# Seal the Deal

<img src="./logo.png"
     alt="Seal the Deal Logo"
     style="float: right; margin-right: 10px;
     margin-top: -150px;"
     width="230px">

## The most awesome game of alltime

## Build Setupgit

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
