import { calculateDistance } from '../../../src/util.js'

describe('A função calculateDistance', function() {
  it('deve calcular distancia entre pontos iguais', function() {
    let a = { xPos: 8, yPos: 5 }
    let b = { xPos: 8, yPos: 5 }

    let distancia = calculateDistance(a, b)

    expect(distancia).to.equal(0)
  })

  it('deve calcular distancia entre pontos de mesmo x', function() {
    let a = { xPos: 2, yPos: 2 }
    let b = { xPos: 2, yPos: 8 }

    let distancia = calculateDistance(a, b)

    expect(distancia).to.equal(6)
  })

  it('deve calcular distancia entre pontos de mesmo y', function() {
    let a = { xPos: 3, yPos: 6 }
    let b = { xPos: 15, yPos: 6 }

    let distancia = calculateDistance(a, b)

    expect(distancia).to.equal(12)
  })

  it('deve calcular distancia entre pontos diferentes', function() {
    let a = { xPos: 2, yPos: 9 }
    let b = { xPos: 16, yPos: 3 }

    let distancia = calculateDistance(a, b)

    expect(distancia).to.equal(15)
  })
})
