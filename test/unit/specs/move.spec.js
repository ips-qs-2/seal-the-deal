import { move } from '../../../src/util.js'

describe('A função move', function() {
  it('nao deve mover um ponto quando a quantidade de casas for menor que 0', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, -1, 'SO')

    expect(ponto.xPos).to.equal(8)
    expect(ponto.yPos).to.equal(5)
  })

  it('nao deve mover um ponto quando a quantidade de casas for igual 0', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, 0, 'SO')

    expect(ponto.xPos).to.equal(8)
    expect(ponto.yPos).to.equal(5)
  })

  it('nao deve mover um ponto quando a direcao for inexistente', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, 1, 'SW')

    expect(ponto.xPos).to.equal(8)
    expect(ponto.yPos).to.equal(5)
  })

  it('deve mover um ponto para norte', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, 1, 'N')

    expect(ponto.xPos).to.equal(8)
    expect(ponto.yPos).to.equal(4)
  })

  it('deve mover um ponto para sul', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, 1, 'S')

    expect(ponto.xPos).to.equal(8)
    expect(ponto.yPos).to.equal(6)
  })

  it('deve mover um ponto para leste', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, 1, 'E')

    expect(ponto.xPos).to.equal(9)
    expect(ponto.yPos).to.equal(5)
  })

  it('deve mover um ponto para oeste', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, 1, 'O')

    expect(ponto.xPos).to.equal(7)
    expect(ponto.yPos).to.equal(5)
  })

  it('deve mover um ponto para nordeste', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, 1, 'NE')

    expect(ponto.xPos).to.equal(9)
    expect(ponto.yPos).to.equal(4)
  })

  it('deve mover um ponto para sudeste', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, 1, 'SE')

    expect(ponto.xPos).to.equal(9)
    expect(ponto.yPos).to.equal(6)
  })

  it('deve mover um ponto para noroeste', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, 1, 'NO')

    expect(ponto.xPos).to.equal(7)
    expect(ponto.yPos).to.equal(4)
  })

  it('deve mover um ponto para sudoeste', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, 1, 'SO')

    expect(ponto.xPos).to.equal(7)
    expect(ponto.yPos).to.equal(6)
  })

  it('deve mover um ponto por n casas', function() {
    let ponto = { xPos: 8, yPos: 5 }

    move(ponto, 2, 'SO')

    expect(ponto.xPos).to.equal(6)
    expect(ponto.yPos).to.equal(7)
  })

  it('nao deve ultrapassar o limite para norte', function() {
    let ponto = { xPos: 8, yPos: 1 }

    move(ponto, 2, 'N')

    expect(ponto.xPos).to.equal(8)
    expect(ponto.yPos).to.equal(0)
  })

  it('nao deve ultrapassar o limite para sul', function() {
    let ponto = { xPos: 8, yPos: 9 }

    move(ponto, 2, 'S')

    expect(ponto.xPos).to.equal(8)
    expect(ponto.yPos).to.equal(10)
  })

  it('nao deve ultrapassar o limite para leste', function() {
    let ponto = { xPos: 15, yPos: 5 }

    move(ponto, 2, 'E')

    expect(ponto.xPos).to.equal(16)
    expect(ponto.yPos).to.equal(5)
  })

  it('nao deve ultrapassar o limite para oeste', function() {
    let ponto = { xPos: 1, yPos: 5 }

    move(ponto, 2, 'O')

    expect(ponto.xPos).to.equal(0)
    expect(ponto.yPos).to.equal(5)
  })

  it('nao deve ultrapassar o limite para nordeste', function() {
    let ponto = { xPos: 17, yPos: 0 }

    move(ponto, 1, 'NE')

    expect(ponto.xPos).to.equal(17)
    expect(ponto.yPos).to.equal(0)
  })

  it('nao deve ultrapassar o limite para sudeste', function() {
    let ponto = { xPos: 17, yPos: 11 }

    move(ponto, 1, 'SE')

    expect(ponto.xPos).to.equal(17)
    expect(ponto.yPos).to.equal(11)
  })

  it('nao deve ultrapassar o limite para noroeste', function() {
    let ponto = { xPos: 0, yPos: 0 }

    move(ponto, 1, 'NO')

    expect(ponto.xPos).to.equal(0)
    expect(ponto.yPos).to.equal(0)
  })

  it('nao deve ultrapassar o limite para sudoeste', function() {
    let ponto = { xPos: 0, yPos: 11 }

    move(ponto, 1, 'SO')

    expect(ponto.xPos).to.equal(0)
    expect(ponto.yPos).to.equal(11)
  })
})
