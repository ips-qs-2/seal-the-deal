import { getDistanceRadius } from '../../../src/util.js'

describe('A função getDistanceRadius', function() {
  it('deve medir a quantidade de casas quando a distancia for menor que o raio', function() {
    let raio = 3
    let distancia = 2

    let casas = getDistanceRadius(raio, distancia)

    expect(casas).to.equal(2)
  })

  it('deve medir a quantidade de casas quando a distancia for igual ao raio', function() {
    let raio = 3
    let distancia = 3

    let casas = getDistanceRadius(raio, distancia)

    expect(casas).to.equal(3)
  })

  it('deve medir a quantidade de casas quando a distancia for maior que o raio', function() {
    let raio = 3
    let distancia = 4

    let casas = getDistanceRadius(raio, distancia)

    expect(casas).to.equal(3)
  })
})
