import { indicateDirection } from '../../../src/util.js'

describe('A função indicateDirection', function() {
  it('nao deve direcionar pontos iguais', function() {
    let origem = { xPos: 4, yPos: 3 }
    let destino = { xPos: 4, yPos: 3 }

    let direcao = indicateDirection(origem, destino)

    expect(direcao).to.be.null
  })

  it('deve direcionar um ponto para norte', function() {
    let origem = { xPos: 4, yPos: 3 }
    let destino = { xPos: 4, yPos: 0 }

    let direcao = indicateDirection(origem, destino)

    expect(direcao).to.equal('N')
  })

  it('deve direcionar um ponto para sul', function() {
    let origem = { xPos: 4, yPos: 3 }
    let destino = { xPos: 4, yPos: 6 }

    let direcao = indicateDirection(origem, destino)

    expect(direcao).to.equal('S')
  })

  it('deve direcionar um ponto para leste', function() {
    let origem = { xPos: 4, yPos: 3 }
    let destino = { xPos: 7, yPos: 3 }

    let direcao = indicateDirection(origem, destino)

    expect(direcao).to.equal('E')
  })

  it('deve direcionar um ponto para oeste', function() {
    let origem = { xPos: 4, yPos: 3 }
    let destino = { xPos: 1, yPos: 3 }

    let direcao = indicateDirection(origem, destino)

    expect(direcao).to.equal('O')
  })

  it('deve direcionar um ponto para nordeste', function() {
    let origem = { xPos: 4, yPos: 3 }
    let destino = { xPos: 6, yPos: 1 }

    let direcao = indicateDirection(origem, destino)

    expect(direcao).to.equal('NE')
  })

  it('deve direcionar um ponto para sudeste', function() {
    let origem = { xPos: 4, yPos: 3 }
    let destino = { xPos: 6, yPos: 5 }

    let direcao = indicateDirection(origem, destino)

    expect(direcao).to.equal('SE')
  })

  it('deve direcionar um ponto para noroeste', function() {
    let origem = { xPos: 4, yPos: 3 }
    let destino = { xPos: 2, yPos: 1 }

    let direcao = indicateDirection(origem, destino)

    expect(direcao).to.equal('NO')
  })

  it('deve direcionar um ponto para sudoeste', function() {
    let origem = { xPos: 4, yPos: 3 }
    let destino = { xPos: 2, yPos: 5 }

    let direcao = indicateDirection(origem, destino)

    expect(direcao).to.equal('SO')
  })
})
