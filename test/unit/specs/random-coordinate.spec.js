import {
  generateUnrestricted,
  generateRestrict
} from '../../../src/util.js'

describe('A função generateUnrestricted', function() {
  it('deve sortear um ponto de forma irrestrita', function() {
    let ponto = generateUnrestricted()

    expect(ponto.xPos).to.be.gte(0)
    expect(ponto.xPos).to.be.lte(17)

    expect(ponto.yPos).to.be.gte(0)
    expect(ponto.yPos).to.be.lte(11)
  })

  it('deve sortear um ponto de forma restrita', function() {
    let ponto = generateRestrict([{ xPos: 5, yPos: 3 }])

    let resultado = ponto.xPos !== 5 || ponto.yPos !== 3
    expect(resultado).to.be.true
  })
})
